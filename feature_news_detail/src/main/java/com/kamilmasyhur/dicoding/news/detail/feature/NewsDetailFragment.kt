package com.kamilmasyhur.dicoding.news.detail.feature

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.browser.customtabs.CustomTabsIntent
import kotlinx.android.synthetic.main.fragment_news_detail.view.*

class NewsDetailFragment : Fragment() {
    private lateinit var newsUrl: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        newsUrl = arguments?.getString(NEWS_URL).toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_news_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.wvNewsDetail.loadUrl(newsUrl)
    }

    companion object {
        private const val NEWS_URL = "news_url"
    }
}
