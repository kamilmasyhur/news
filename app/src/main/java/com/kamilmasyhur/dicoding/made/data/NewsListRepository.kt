package com.kamilmasyhur.dicoding.made.data

import com.kamilmasyhur.dicoding.lib.model.NewsModel
import com.kamilmasyhur.dicoding.lib.model.Response
import com.kamilmasyhur.dicoding.made.data.network.NewsListApiDataSource
import kotlinx.coroutines.flow.Flow

interface NewsListRepository {
    suspend fun getHeadLinesNewsIndonesia(): Flow<Response<NewsModel>>
}

class NewsListRepositoryImpl (
    private val newsListApiDataSource: NewsListApiDataSource
): NewsListRepository {
    override suspend fun getHeadLinesNewsIndonesia(): Flow<Response<NewsModel>> {
        return newsListApiDataSource.getHeadLinesNewsIndonesia()
    }

}
