package com.kamilmasyhur.dicoding.made.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kamilmasyhur.dicoding.lib.model.Response
import com.kamilmasyhur.dicoding.made.domain.NewsListUseCase
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class NewsListViewModel(
    private val newsListUseCase: NewsListUseCase
) : ViewModel() {
    private val _state: MutableLiveData<NewsListState> = MutableLiveData()
    val state: LiveData<NewsListState> = _state

    fun getHeadLineNewsIndonesia() {
        viewModelScope.launch {
            _state.value = NewsListState.Loading
            newsListUseCase.getHeadLinesNewsIndonesia()
                .collect {
                    if (it is Response.Success) {
                        _state.value = NewsListState.Content(it.data)
                    } else {
                        _state.value = NewsListState.Error
                    }
                }
        }
    }
}
