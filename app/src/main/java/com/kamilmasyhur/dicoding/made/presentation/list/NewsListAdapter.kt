package com.kamilmasyhur.dicoding.made.presentation.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kamilmasyhur.dicoding.made.R
import com.kamilmasyhur.dicoding.lib.model.NewsArticle
import com.kamilmasyhur.dicoding.made.presentation.list.cardtype.NewsCard

class NewsListAdapter(
    private val articleList: MutableList<NewsArticle> = mutableListOf()
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_news, parent, false)
        return NewsCard(view)
    }

    override fun getItemCount(): Int = articleList.count()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as NewsCard).bind(articleList[position])
    }

    fun addNews(list: List<NewsArticle>) {
        articleList.clear()
        articleList.addAll(list)
        notifyDataSetChanged()
    }
}
