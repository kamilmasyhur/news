package com.kamilmasyhur.dicoding.made.domain

import android.annotation.SuppressLint
import com.kamilmasyhur.dicoding.lib.model.NewsArticle
import com.kamilmasyhur.dicoding.lib.model.NewsModel
import com.kamilmasyhur.dicoding.lib.model.Response
import java.text.SimpleDateFormat

interface NewsListTransformer {
    suspend fun apply(result: Response<NewsModel>): Response<List<NewsArticle>>
}

class NewsListTransformerImpl : NewsListTransformer {
    override suspend fun apply(result: Response<NewsModel>): Response<List<NewsArticle>> {
        val listOfArticle = mutableListOf<NewsArticle>()
        if (result is Response.Success) {
            result.data.articles.forEach { resultArticle ->
                listOfArticle.add(
                    NewsArticle(
                        author = resultArticle.author,
                        title = resultArticle.title,
                        description = resultArticle.description,
                        url = resultArticle.url,
                        urlToImage = resultArticle.urlToImage,
                        publishedAt = transformToIndonesianDateTime(resultArticle.publishedAt),
                        content = resultArticle.content,
                        source = resultArticle.source.name
                    )
                )
            }
        }
        return Response.Success(listOfArticle)
    }

    @SuppressLint("SimpleDateFormat")
    private fun transformToIndonesianDateTime(publishedAt: String): String {
        val inputFormat = SimpleDateFormat(SERVER_DATE_TIME_PATTERN)
        val outputFormat = SimpleDateFormat(MOBILE_DATE_TIME_PATTERN)
        return outputFormat.format(inputFormat.parse(publishedAt))
    }

    companion object {
        const val SERVER_DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        const val MOBILE_DATE_TIME_PATTERN = "dd MMM yyyy HH:mm"
    }
}
