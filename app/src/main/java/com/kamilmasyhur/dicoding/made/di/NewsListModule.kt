package com.kamilmasyhur.dicoding.made.di

import com.kamilmasyhur.dicoding.common.network.NewsNetworkApi
import com.kamilmasyhur.dicoding.made.data.NewsListRepository
import com.kamilmasyhur.dicoding.made.data.NewsListRepositoryImpl
import com.kamilmasyhur.dicoding.made.data.network.NewsListApiDataSource
import com.kamilmasyhur.dicoding.made.data.network.NewsListApiDataSourceImpl
import com.kamilmasyhur.dicoding.made.domain.NewsListTransformer
import com.kamilmasyhur.dicoding.made.domain.NewsListTransformerImpl
import com.kamilmasyhur.dicoding.made.domain.NewsListUseCase
import com.kamilmasyhur.dicoding.made.domain.NewsListUseCaseImpl
import com.kamilmasyhur.dicoding.made.presentation.NewsListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val newsListModule = module {
    single<NewsListApiDataSource> {
        NewsListApiDataSourceImpl(get())
    }
    single<NewsListRepository> {
        NewsListRepositoryImpl(get())
    }
    single<NewsListTransformer> {
        NewsListTransformerImpl()
    }

    single<NewsListUseCase> { NewsListUseCaseImpl(get(), get()) }

    viewModel {
        NewsListViewModel(get())
    }
}
