package com.kamilmasyhur.dicoding.made.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.kamilmasyhur.dicoding.made.R
import com.kamilmasyhur.dicoding.made.presentation.list.NewsListAdapter
import kotlinx.android.synthetic.main.fragment_news_list.rvNewsList
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewsListFragment : Fragment() {
    private val newsListViewModel: NewsListViewModel by viewModel()
    private val newsListAdapter: NewsListAdapter by lazy {
        NewsListAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_news_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rvNewsList.adapter = newsListAdapter

        newsListViewModel.state.observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                is NewsListState.Loading -> {
                    Toast.makeText(context, "Loading", Toast.LENGTH_LONG).show()
                }
                is NewsListState.Content -> {
                    Toast.makeText(context, "Content", Toast.LENGTH_LONG).show()
                    newsListAdapter.addNews(state.articleList)
                }
                is NewsListState.Error -> {
                    Toast.makeText(context, "Error", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    override fun onStart() {
        super.onStart()
        newsListViewModel.getHeadLineNewsIndonesia()
    }

    companion object {
        @JvmStatic
        fun newInstance() = NewsListFragment()
    }
}
