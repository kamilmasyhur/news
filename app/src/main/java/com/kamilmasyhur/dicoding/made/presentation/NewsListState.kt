package com.kamilmasyhur.dicoding.made.presentation

import com.kamilmasyhur.dicoding.lib.model.NewsArticle

sealed class NewsListState {
    object Loading : NewsListState()
    data class Content(
        val articleList: List<NewsArticle>
    ) : NewsListState()

    object Error : NewsListState()
}
