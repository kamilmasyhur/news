package com.kamilmasyhur.dicoding.made

import android.app.Application
import com.kamilmasyhur.dicoding.common.network.di.networkModule
import com.kamilmasyhur.dicoding.made.di.newsListModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module

class NewsApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@NewsApplication)
            // declare modules
            modules(myModule)
        }

    }
}

val myModule = listOf(
    networkModule,
    newsListModule
)

object NewsScope {
    const val NEWS_LIST = "NEWS_LIST"
}
