package com.kamilmasyhur.dicoding.made.presentation.list.cardtype

import android.view.View
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.kamilmasyhur.dicoding.lib.model.NewsArticle
import com.kamilmasyhur.dicoding.made.R
import com.kamilmasyhur.dicoding.made.presentation.NewsListFragmentDirections
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_news.view.*

class NewsCard(private val view: View): RecyclerView.ViewHolder(view) {
    fun bind(article: NewsArticle) {
        Picasso.get().load(article.urlToImage)
            .placeholder(R.drawable.ic_photo_camera)
            .into(view.ivCardNewsImage)

        view.tvCardNewsTitle.text = article.title
        view.tvCardNewsDescription.text = article.description
        view.tvCardNewsDateTime.text = article.publishedAt

        setFavoriteImage(article)

        setOnClickListener(article)
    }

    private fun setOnClickListener(article: NewsArticle) {
        view.cvNewsHolder.setOnClickListener {
            val action = NewsListFragmentDirections.actionNewsListFragmentToNewsDetailFragment(article.url)
            view.findNavController().navigate(action)
        }

        view.ivFavoriteNewsItem.setOnClickListener {
            article.favorite = !article.favorite
            setFavoriteImage(article)
        }
    }

    private fun setFavoriteImage(article: NewsArticle) {
        if (article.favorite) {
            view.ivFavoriteNewsItem.setImageResource(R.drawable.ic_favorite)
        } else {
            view.ivFavoriteNewsItem.setImageResource(R.drawable.ic_favorite_border)
        }
    }
}
