package com.kamilmasyhur.dicoding.made.domain

import com.kamilmasyhur.dicoding.lib.model.NewsArticle
import com.kamilmasyhur.dicoding.lib.model.Response
import com.kamilmasyhur.dicoding.made.data.NewsListRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

interface NewsListUseCase {
    suspend fun getHeadLinesNewsIndonesia(): Flow<Response<List<NewsArticle>>>
}

class NewsListUseCaseImpl(
    private val repository: NewsListRepository,
    private val newsListTransformer: NewsListTransformer
) : NewsListUseCase {

    override suspend fun getHeadLinesNewsIndonesia(): Flow<Response<List<NewsArticle>>> =
        repository.getHeadLinesNewsIndonesia().map {
            newsListTransformer.apply(it)
        }

}
