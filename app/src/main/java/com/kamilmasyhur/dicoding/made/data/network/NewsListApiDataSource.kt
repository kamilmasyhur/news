package com.kamilmasyhur.dicoding.made.data.network

import com.kamilmasyhur.dicoding.common.network.NewsNetworkApi
import com.kamilmasyhur.dicoding.lib.model.NewsModel
import com.kamilmasyhur.dicoding.lib.model.Response
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

interface NewsListApiDataSource {
    suspend fun getHeadLinesNewsIndonesia(): Flow<Response<NewsModel>>
}

class NewsListApiDataSourceImpl(
    private val apiDataSource: NewsNetworkApi
):NewsListApiDataSource {
    override suspend fun getHeadLinesNewsIndonesia(): Flow<Response<NewsModel>> {
        return flow {
            try {
                val response = apiDataSource.getHeadLineNewsIndonesia()
                emit(Response.Success(response))
            } catch (exception: Exception) {
                emit(Response.Error(exception))
            }
        }.flowOn(Dispatchers.IO)
    }
}
