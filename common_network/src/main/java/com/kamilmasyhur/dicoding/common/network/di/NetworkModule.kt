package com.kamilmasyhur.dicoding.common.network.di

import com.kamilmasyhur.dicoding.common.network.NewsNetwork
import org.koin.dsl.module

val networkModule = module {
    single { NewsNetwork().provideForecastApi() }
}
