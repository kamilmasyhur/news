package com.kamilmasyhur.dicoding.common.network

import com.kamilmasyhur.dicoding.common.network.BuildConfig.SERVER_URL
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class NewsNetwork {
    fun provideForecastApi(): NewsNetworkApi = createRetrofit().create(
        NewsNetworkApi::class.java)

    private fun createRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl(SERVER_URL)
            .client(okHttpClient(AuthInterceptor()))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun okHttpClient(authInterceptor: AuthInterceptor): OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .addInterceptor(authInterceptor)
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .build()
    }
}

class AuthInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var req = chain.request()
        // this api key is just for dicoding purpose only to make it faster
        val url = req.url.newBuilder().addQueryParameter("apiKey", "20c860eb74c3400f9d26ce65b2e84575").build()
        req = req.newBuilder().url(url).build()
        return chain.proceed(req)
    }
}
