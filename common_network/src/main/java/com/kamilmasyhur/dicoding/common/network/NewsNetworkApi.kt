package com.kamilmasyhur.dicoding.common.network

import com.kamilmasyhur.dicoding.lib.model.NewsModel
import com.kamilmasyhur.dicoding.lib.model.Response
import retrofit2.http.GET

interface NewsNetworkApi {
    @GET("/v2/top-headlines?country=id")
    suspend fun getHeadLineNewsIndonesia(): NewsModel
}
