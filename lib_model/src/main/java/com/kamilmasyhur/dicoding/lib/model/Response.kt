package com.kamilmasyhur.dicoding.lib.model

sealed class Response<out R> {
    data class Success<out D>(val data: D) : Response<D>()
    data class Error(val throwable: Throwable?) : Response<Nothing>()
}
